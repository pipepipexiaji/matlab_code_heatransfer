# To do:
 - Scaling theory
 - Casimir forces between infinite-area

 -------------------------------------

This repotory contains almost all the codes for project 1 of Heat Transfer ME2017

Those codes are also parts of the ongoing personal project: multiscale FF for biopolymers & electrical fluids & electromagenetic fields

---
# Project 1 of Heat Transfer
###1


The heat diffusion equation can be simplified to make it easier to handle. We will now examine a 2D steady state system with no internal generation. This gets rid of the right hand side of the equation, the z term, and the internal generation term.
$$ \frac{\partial}{\partial x} ( k \frac{\partial T}{\partial x}) + \frac{\partial}{\partial y} ( k \frac{\partial T}{\partial y} ) = 0 $$

This can be simplified by dividing the whole expression through by k:

$$ \frac{\partial^2 T}{\partial y^2} = \frac{T_{i, j+1} - 2 T_{i, j} + T_{i, j-1}}{\Delta y^2} $$

Now that the equation has been simplified, we will continue with relaxation method to discretize the governing equation of finite differences with m points.

$$ \frac{\partial^2 T}{\partial x^2} = \frac{T_{i+1, j} - 2 T_{i, j} + T_{i-1, j}}{\Delta x^2} $$

$$ \frac{\partial^2 T}{\partial y^2} = \frac{T_{i, j+1} - 2 T_{i, j} + T_{i, j-1}}{\Delta y^2} $$

$$ \frac{T_{i+1, j} + 2 T_{i, j} + T_{i-1, j}}{\Delta x^2} + \frac{T_{i, j+1} + 2 T_{i, j} + T_{i, j-1}}{\Delta y^2} = 0 $$

$$ \because  \Delta x =\Delta y$$

Now, the above equation can be simplified to be:
\begin{equation}
    T_{i+1, j} + T_{i-1, j} + T_{i, j+1} + T_{i, j-1} - 4 T{i, j} = 0
\end{equation}

If we look more closely at the above equation, it is clear to find that the temperature of any given block is the average of the blocks around it.

    import numpy as np
    import matplotlib.pyplot as plt
    %matplotlib inline

#### **1.2**. Two Dimensional Steady State Heat Diffusion
**Solving for steady state using matricies**

Since we do not know any of the temperatures we must simultaneously solve for the temperatures using matrix operations

Since we do not know any of the temperatures we must simultaneously solve for the temperatures using matrix operations

For the sake of convenience let's say that you discretize a solid into a 5x5 matrix. This means that you will have a temperature vector that is 16x1 that you are trying to solve for. Let's start to write some equations to figure out how we will solve for the temperature matrix:
$$4 T_0 = T_1 + T_5 + T_{surr} + T_{surr}$$$$4 T_1 = T_2 + T_6 + T_0 + T_{surr}$$$$4 T_2 = T_2 + T_3 + T_7 + T_{surr}$$
If we move toward the middle of the solid, the pattern is a bit more cohesive: $$4 T_6 = T_5 + T_7 + T_{11} + T_1$$
A matrix patern begins to emerge:

\begin{bmatrix} -4 & 1 & 0 & 0 & 1 & \dots  & 0 \\\\\
    1 & -4 & 1 & 0 & 0 & \\dots  & 0 \\\\\
    0 & 1 & -4 & 1 & 0 & \\dots  & 0 \\\\\
    \\vdots & \\vdots & \\vdots & \\vdots & \\vdots & \\ddots & 1 \\\\\
0 & 0 & 0 & \\dots & 0 & 1 & -4\
    \\end{bmatrix}

A matrix of temperature coefficients, A, and a vector of constants, C, can be formed after observing the pattern in the form

$ A \cdot \vec{T} = \vec{C} $
And T can be solved for using:
$$ \vec{T} = A^{-1} \vec{C} $$

     def create_A(n):
        A = np.zeros((n*n,n*n))
        for i in range(n*n)
            A[i,i] = -4
            if i % n == n-1 and i+1 < n*n:
                A[i,i+1] = 0
            elif i+1 < n*n:
                A[i,i+1] = 1
            if i % n != 0:
                A[i,i-1] = 1
            if i + n < n*n:
                A[i,i+n] = 1
            if i - n >= 0:
                A[i,i-n] = 1
        return A
---

    N = 4
    A = create_A(N)
    print (A)

```
[-4.  1.  0.  0.  1.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.]
[ 1. -4.  1.  0.  0.  1.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.]
[ 0.  1. -4.  1.  0.  0.  1.  0.  0.  0.  0.  0.  0.  0.  0.  0.]
[ 0.  0.  1. -4.  0.  0.  0.  1.  0.  0.  0.  0.  0.  0.  0.  0.]
[ 1.  0.  0.  0. -4.  1.  0.  0.  1.  0.  0.  0.  0.  0.  0.  0.]
[ 0.  1.  0.  0.  1. -4.  1.  0.  0.  1.  0.  0.  0.  0.  0.  0.]
[ 0.  0.  1.  0.  0.  1. -4.  1.  0.  0.  1.  0.  0.  0.  0.  0.]
[ 0.  0.  0.  1.  0.  0.  1. -4.  0.  0.  0.  1.  0.  0.  0.  0.]
[ 0.  0.  0.  0.  1.  0.  0.  0. -4.  1.  0.  0.  1.  0.  0.  0.]
[ 0.  0.  0.  0.  0.  1.  0.  0.  1. -4.  1.  0.  0.  1.  0.  0.]
[ 0.  0.  0.  0.  0.  0.  1.  0.  0.  1. -4.  1.  0.  0.  1.  0.]
[ 0.  0.  0.  0.  0.  0.  0.  1.  0.  0.  1. -4.  0.  0.  0.  1.]
[ 0.  0.  0.  0.  0.  0.  0.  0.  1.  0.  0.  0. -4.  1.  0.  0.]
[ 0.  0.  0.  0.  0.  0.  0.  0.  0.  1.  0.  0.  1. -4.  1.  0.]
[ 0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  1.  0.  0.  1. -4.  1.]
[ 0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  1.  0.  0.  1. -4.]]
```

Now that there is a temperature matrix, there must also be a constants matrix to account for the surrounding temperature at the B.C.

####1.3 B.C. Analysis
```
def create_C(n, T_x, T_y):
    C = np.zeros((n*n, 1))
    for i in range(n*n):
        if i%n == n-1 or i%n ==0:
            C[i,0] = -T_y
        elif i > (n*n)-n:
            C[i,0] = -T_x
        elif i < n:
            C[i,0] = -T_x
        if i==0 or i==n-1:
            C[i,0] = -2*T_x
        if i==(n*n)-1 or i==(n*n)-n:
            C[i,0] = -T_x-T_y

    return C
```

Erm, just find relaxation method required....
