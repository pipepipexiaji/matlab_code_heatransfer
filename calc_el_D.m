% THERMAL2D Two dimensional finite element thermal problem solver for the multiscale FF applied to polyelectrolytes or biopolymer modeling
calc_el_D.m
calc_el_stress.m
calc_nd_stress.m
defval.m
det2D.m
eig2d.m
elastic2d_test.m
elastic2d.m
generate_mesh.m
inv2D.m
ip_triangle.m
mechanical2d_test.m
mechanical2d.m
mesher_test.m
pathdef.m
plot2d_strain_cross.m
shp_deriv_triangle.m
thermal2d_test.m
thermal2d.m
tri_area.m
function [ D Dn ] = calc_el_D(Mu,nu,plane_strain)
%
% calculate a 2D linear isotropic elasticity material matrix
%
% returns the matrix normalized to the (3,3) element
%
n=size(Mu,1);
m=size(nu,1);
if(n ~= m)
        error('mismatch of second and first dimension in calc_el_D');
end

D = [];
Dn = [];

Dloc = zeros(3,3);
Dloc(1,1) = 1;
Dloc(2,2) = 1;

for i =1 : n
    if(plane_strain)
        tmp = nu(i)/(1.-nu(i));
        Dloc(1,2) = tmp;
        Dloc(2,1) = tmp;
        Dloc(3,3) = (1-2.*nu(i))/(2*(1-nu(i)));
        fac = Mu(i)*(1-nu(i))/((1+nu(i))*(1-2*nu(i)));
    else % plane stress
        Dloc(1,2) = nu(i);
        Dloc(2,1) = nu(i);
        Dloc(3,3) = (1-nu(i))/2;
        fac = Mu(i)/(1-nu(i)^2);
    end
    Dn(i)    = fac * Dloc(3,3);
    D(i,:,:) = fac * Dloc/Dn(i);
end
if i == 1
    D = reshape(D,3,3);
end
